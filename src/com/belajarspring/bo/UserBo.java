package com.belajarspring.bo;

import java.util.List;

import com.belajarspring.service.User;

public interface UserBo {
	public void insert(User user);
	public User findByUserId(int usrId);
	public List<User> findAllUsers();
	public void update(User user);
	public void delete(int usrId);
}

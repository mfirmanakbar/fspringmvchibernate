package com.belajarspring.bo.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.belajarspring.bo.UserBo;
import com.belajarspring.dao.UserDao;
import com.belajarspring.service.User;

public class UserBoImpl implements UserBo{
	
	@Autowired
	private UserDao usrDao;
		
	public UserDao getUsrDao() {
		return usrDao;
	}

	public void setUsrDao(UserDao usrDao) {
		this.usrDao = usrDao;
	}

	@Override
	public void insert(User user) {
		usrDao.insert(user);
	}

	@Override
	public User findByUserId(int usrId) {
		return usrDao.findByUserId(usrId);
	}

	@Override
	public List<User> findAllUsers() {
		return usrDao.findAllUsers();
	}

	@Override
	public void update(User user) {
		usrDao.update(user);
	}

	@Override
	public void delete(int usrId) {
		usrDao.delete(usrId);
	}

}

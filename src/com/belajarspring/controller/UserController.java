package com.belajarspring.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.belajarspring.bo.UserBo;
import com.belajarspring.service.User;
import com.belajarspring.service.UserForm;

@Controller
public class UserController {
	
	@Autowired
	private UserBo userBo;
	
	@RequestMapping(value="/list-user", method=RequestMethod.GET)
	public String listCustomer(Model m) {	
		m.addAttribute("userku", userBo.findAllUsers());
        return "user-list";
	}
	
	@RequestMapping(value="/create-user", method=RequestMethod.GET)
	public String prepareCreate(Model m) {
		UserForm uf = new UserForm();
		m.addAttribute("err_message", "");
		m.addAttribute("success_message", "");
		m.addAttribute("usr_id_ctrl", 0);
		uf.setFlag("CREATE");
		m.addAttribute("userForm", uf);
		return "user-form";
	}
	
	@RequestMapping(value="/create-user", method=RequestMethod.POST)
	public String submitCreate(@ModelAttribute @Valid UserForm usr, BindingResult result, Model m) {
		String flag = usr.getFlag();
		m.addAttribute("err_message", "");
		m.addAttribute("success_message", "");
		if(flag.equals("CREATE"))
			m.addAttribute("usr_id_ctrl", 0);
		else
			m.addAttribute("usr_id_ctrl", usr.getUsr_id());
		
		if(!result.hasErrors()) {
			User customer = new User(
					usr.getUsr_id(), 
					usr.getUsr_username(), 
					usr.getUsr_password(), 
					usr.getUsr_status()
				);
	    	
			try {				
				if(flag.equals("CREATE")) {
					userBo.insert(customer);
					m.addAttribute("success_message", "Successfully creating Customer: " + usr.getUsr_username());
				}
				else {
					userBo.update(customer);
					m.addAttribute("success_message", "Successfully updating Customer: " + usr.getUsr_password());
				}
				
				m.addAttribute("usr_id_ctrl", usr.getUsr_id());
				usr.setFlag("UPDATE");
			}
			catch (Exception e) {
				m.addAttribute("err_message", e.getMessage() );						
			}
		}
		return "user-form";
	}
	
	
	@RequestMapping(value = { "/delete-user-{usr_id}" }, method = RequestMethod.GET)
    public String submitDelete(@PathVariable int userId) {
		userBo.delete(userId);
        return "redirect:/list-user";
    }
	
	//perhatikan penulisan usr_id pada value harus sama seperti usr_id pada pathvariable int usr_id di bawah ini
	@RequestMapping(value = { "/update-user-{usr_id}" }, method = RequestMethod.GET) 
    public String prepareUpdate(@PathVariable int usr_id, Model m) { 
		User cust = userBo.findByUserId(usr_id); 
		UserForm usx = new UserForm();
		usx.setUsr_id(usr_id);
		usx.setUsr_username(cust.getUsr_username());
		usx.setUsr_password(cust.getUsr_password());
		usx.setUsr_status(cust.getUsr_status());		
		usx.setFlag("EDIT");
		m.addAttribute("err_message", "");
		m.addAttribute("success_message", "");
		m.addAttribute("usr_id_ctrl", usr_id);
		m.addAttribute("userForm", usx);
		return "user-form";
    }

	
	@RequestMapping(value={ "/update-user-{usr_id}" }, method=RequestMethod.POST)
	public String submitUpdate(@ModelAttribute @Valid UserForm usrx, BindingResult result, Model m) {
		m.addAttribute("err_message", "");
		m.addAttribute("success_message", "");
		m.addAttribute("usr_id_ctrl", usrx.getUsr_id());
		
		if(!result.hasErrors()) {
			User customer = new User(
					usrx.getUsr_id(),
					usrx.getUsr_username(), 
					usrx.getUsr_password(),
					usrx.getUsr_status()
				);
	    	
			try {				
				userBo.update(customer);
				m.addAttribute("success_message", "Successfully updating Customer: " + usrx.getUsr_username());				
				m.addAttribute("usr_id_ctrl", usrx.getUsr_id());
				usrx.setFlag("UPDATE");
			}
			catch (Exception e) {
				m.addAttribute("err_message", e.getMessage() );
			}
		}
		return "user-form";
	}
	
}

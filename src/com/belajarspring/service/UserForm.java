package com.belajarspring.service;

import org.hibernate.validator.constraints.*;
import javax.validation.constraints.*;

public class UserForm {
	
	@NotNull
	@Range(min=1, max=1000, message="\"User ID\" tidak valid")
	int usr_id;
	
	@NotBlank (message="\"Username\" harus diisi")
	String usr_username;
	
	@NotBlank (message="\"Password\" harus diisi")
	String usr_password;
	
	@NotBlank (message="\"Status\" harus diisi")
	String usr_status;
	private String flag;
	
	public UserForm() {
		super();
	}
	public UserForm(int usr_id, String usr_username, String usr_password, String usr_status) {
		super();
		this.usr_id = usr_id;
		this.usr_username = usr_username;
		this.usr_password = usr_password;
		this.usr_status = usr_status;
	}
	public int getUsr_id() {
		return usr_id;
	}
	public void setUsr_id(int usr_id) {
		this.usr_id = usr_id;
	}
	public String getUsr_username() {
		return usr_username;
	}
	public void setUsr_username(String usr_username) {
		this.usr_username = usr_username;
	}
	public String getUsr_password() {
		return usr_password;
	}
	public void setUsr_password(String usr_password) {
		this.usr_password = usr_password;
	}
	public String getUsr_status() {
		return usr_status;
	}
	public void setUsr_status(String usr_status) {
		this.usr_status = usr_status;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	
}

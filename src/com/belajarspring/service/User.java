package com.belajarspring.service;

import java.io.Serializable;

public class User implements Serializable {
	private static final long serialVersionUID = 2256994432208926628L;
	
	int usr_id;
	String usr_username;
	String usr_password;
	String usr_status;
	public User() {
		super();
	}
	public User(int usr_id, String usr_username, String usr_password, String usr_status) {
		super();
		this.usr_id = usr_id;
		this.usr_username = usr_username;
		this.usr_password = usr_password;
		this.usr_status = usr_status;
	}
	public int getUsr_id() {
		return usr_id;
	}
	public void setUsr_id(int usr_id) {
		this.usr_id = usr_id;
	}
	public String getUsr_username() {
		return usr_username;
	}
	public void setUsr_username(String usr_username) {
		this.usr_username = usr_username;
	}
	public String getUsr_password() {
		return usr_password;
	}
	public void setUsr_password(String usr_password) {
		this.usr_password = usr_password;
	}
	public String getUsr_status() {
		return usr_status;
	}
	public void setUsr_status(String usr_status) {
		this.usr_status = usr_status;
	}
//	public static long getSerialversionuid() {
//		return serialVersionUID;
//	}
	
	
}

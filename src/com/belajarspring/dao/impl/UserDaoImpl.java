package com.belajarspring.dao.impl;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.belajarspring.dao.UserDao;
import com.belajarspring.service.User;

public class UserDaoImpl implements UserDao{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	@Override
	public void insert(User user) {
		this.sessionFactory.getCurrentSession().save(user);
	}

	//bagian ini nama tabel dan kolom sesuaikan dengan User.java pada com.belajarspring.service
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public User findByUserId(int usrId) {
		List<User> list = this.sessionFactory.getCurrentSession()
                .createQuery("from User where usr_id=:usr_id")
                .setParameter("usr_id", usrId)
                .list(); 	
		return (User)list.get(0);
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<User> findAllUsers() {
		List<User> usrList = new ArrayList<User>();
		usrList = this.sessionFactory.getCurrentSession()
                .createQuery("from User")
                .list(); 	
		return usrList;
	}

	@Transactional
	@Override
	public void update(User user) {
		this.sessionFactory.getCurrentSession().update(user);
	}

	@Transactional
	@Override
	public void delete(int usrId) {
		User user = findByUserId(usrId);
		this.sessionFactory.getCurrentSession().delete(user);
	}

}

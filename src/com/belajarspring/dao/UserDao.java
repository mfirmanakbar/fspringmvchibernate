package com.belajarspring.dao;

import java.util.List;

import com.belajarspring.service.User;

public interface UserDao {
	public void insert(User user);
	public User findByUserId(int usrId);
	public List<User> findAllUsers();
	public void update(User user);
	public void delete(int usrId);
}

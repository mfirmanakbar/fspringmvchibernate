<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Form</title>
</head>
<body>

	<c:if test="${not empty err_message}">
		<h3>
			<font color="red">${err_message}</font>
		</h3>
	</c:if>
	
	<c:if test="${not empty success_message}">
		<h3>
			<font color="blue">${success_message}</font>
		</h3>
	</c:if>

	<form:form commandName="userForm">
		<label>User ID: </label>
		<c:if test="${usr_id_ctrl==0}">
			<form:input path="usr_id" />
			<form:errors path="usr_id" />
		</c:if>
		<c:if test="${usr_id_ctrl!=0}">
			<form:hidden path="usr_id" />
			${usr_id_ctrl}
		</c:if>
		<br />

		<label>Username : </label>
		<form:input path="usr_username" size="50" />
		<form:errors path="usr_username" />
		<br />

		<label>Password: </label>
		<form:input path="usr_password" size="50" />
		<form:errors path="usr_password" />
		<br />

		<label>Status: </label>
		<form:input path="usr_status" size="50" />
		<form:errors path="usr_status" />
		<br />

		<form:hidden path="flag" />

		<input type="submit" value="Submit" />
	</form:form>
	<p>
		<a href="index.jsp">Main Menu</a>
</body>
</html>
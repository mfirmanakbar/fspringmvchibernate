<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User List</title>
</head>
<body>
	<div>
		<table border="1">
			<thead>
				<tr>
					<th>ID</th>
					<th>Username</th>
					<th>Password</th>
					<th>Status</th>
					<th>Act</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${userku}" var="user_nya">
					<tr>
						<td>${user_nya.getUsr_id()}</td>
						<td>${user_nya.getUsr_username()}</td>
						<td>${user_nya.getUsr_password()}</td>
						<td>${user_nya.getUsr_status()}</td>
						<td>
							<a href="<c:url value='/update-user-${user_nya.getUsr_id()}' />">edit</a>
							<a href="<c:url value='/delete-user-${user_nya.getUsr_id()}' />">delete</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<div>
		<p>
			<a href="index.jsp">Main Menu</a>
	</div>
</body>
</html>